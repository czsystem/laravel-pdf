<?php

namespace CloudZentral\PDF\DataTemplates\InvoiceDataTemplate;

/**
 * Class InvoiceOrderLine
 * @package CloudZentral\PDF\DataTemplates\InvoiceDataTemplate
 */
class InvoiceOrderLine
{
    /**
     * @var string
     */
    public $item_number;

    /**
     * @var string
     */
    public $text;

    /**
     * @var int
     */
    public $amount;

    /**
     * @var string formatted price string
     */
    public $price;

    /**
     * @var string formatted sub total string
     */
    public $subTotal;

    /**
     * InvoiceOrderLine constructor.
     * @param string $item_number
     * @param string $text
     * @param int $amount
     * @param string $price
     * @param string $subTotal
     */
    public function __construct(string $item_number, string $text, int $amount, string $price, string $subTotal)
    {
        $this->item_number = $item_number;
        $this->text = $text;
        $this->amount = $amount;
        $this->price = $price;
        $this->subTotal = $subTotal;
    }
}
