<?php

namespace CloudZentral\PDF;

use CloudZentral\PDF\Interfaces\PDFInterface;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\Response;

/**
 * Class PDF
 * @package CloudZentral\PDF
 */
class PDF implements PDFInterface
{
    /**
     * @var string
     */
    private $filename;

    /**
     * @var string
     */
    private $html;

    /**
     * PDF constructor.
     * @param string $filename
     * @param string $html
     */
    public function __construct(string $filename, string $html)
    {
        $this->filename = $filename;
        $this->html = $html;
    }

    /**
     * Get PDF-
     * @return \Barryvdh\DomPDF\PDF
     * @throws BindingResolutionException
     */
    private function getPDF(): \Barryvdh\DomPDF\PDF
    {
        return app()->make('dompdf.wrapper');
    }

    /**
     * @inheritDoc
     */
    public function output(): string
    {
        $pdf = $this->getPDF();
        $pdf->loadHTML($this->html);
        return $pdf->output();
    }

    /**
     * @inheritDoc
     */
    public function download(): Response
    {
        $pdf = $this->getPDF();
        $pdf->loadHTML($this->html);
        return $pdf->download($this->filename);
    }

    /**
     * @inheritDoc
     */
    public function save(): void
    {
        $pdf = $this->getPDF();
        $pdf->loadHTML($this->html);
        $pdf->save($this->filename);
    }

    /**
     * @inheritDoc
     */
    public function stream(): Response
    {
        $pdf = $this->getPDF();
        $pdf->loadHTML($this->html);
        return $pdf->stream($this->filename);
    }
}
