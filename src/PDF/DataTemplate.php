<?php

namespace CloudZentral\PDF;

use CloudZentral\PDF\DataTemplates\InvoiceDataTemplate;
use CloudZentral\Templates\Template;

/**
 * Class DataTemplate
 * @package CloudZentral\PDF
 */
abstract class DataTemplate extends Template
{
    /**
     * TYPES.
     */
    const TYPE_INVOICE = "1";

    /**
     * @inheritDoc
     */
    public static function make(string $type)
    {
        switch ($type) {
            case self::TYPE_INVOICE:
                return new InvoiceDataTemplate();
            default:
                return null;
        }
    }
}
