<?php

namespace CloudZentral\PDF;

use CloudZentral\PDF\PDFTemplates\DefaultPDFTemplate;
use CloudZentral\Templates\Template;
use CloudZentral\Templates\Traits\Widgetable;

/**
 * Class PDFTemplate
 * @package CloudZentral\PDF
 */
abstract class PDFTemplate extends Template
{
    use Widgetable;

    /**
     * TYPES.
     */
    const TYPE_DEFAULT = "1";

    /**
     * @inheritDoc
     */
    public static function make(string $type)
    {
        switch($type) {
            case self::TYPE_DEFAULT:
                return new DefaultPDFTemplate();
            default:
                return null;
        }
    }
}
