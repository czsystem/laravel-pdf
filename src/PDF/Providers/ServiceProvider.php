<?php

namespace CloudZentral\PDF\Providers;

/**
 * Class ServiceProvider
 * @package CloudZentral\PDF\Providers
 */
class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../Views', 'laravel-pdf');
    }
}
