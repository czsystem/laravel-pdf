<?php

namespace CloudZentral\PDF\PDFTemplates;

use CloudZentral\PDF\PDFTemplate;
use Illuminate\View\View;

class DefaultPDFTemplate extends PDFTemplate
{
    /**
     * @inheritDoc
     */
    public function getView(array $attributes)
    {
        return view('laravel-pdf::pdf_templates.default', $attributes);
    }

    /**
     * @inheritDoc
     */
    public function getDefaultAttributes(): array
    {
        return [
            'top' => [
                'backgroundColor' => '#ffffff',
                'color' => '#000000',
                'column1' => [
                    'paddingTop' => "0",
                    'paddingBottom' => "0",
                    'paddingLeft' => "0",
                    'paddingRight' => "0",
                    'textAlign' => "left",
                    'verticalAlign' => "top",
                    'img' => [
                        'width' => "100%",
                        'height' => "auto"
                    ],
                    'content' => null
                ],
                'column2' => [
                    'paddingTop' => "0",
                    'paddingBottom' => "0",
                    'paddingLeft' => "0",
                    'paddingRight' => "0",
                    'textAlign' => "left",
                    'verticalAlign' => "top",
                    'img' => [
                        'width' => "100%",
                        'height' => "auto"
                    ],
                    'content' => null
                ],
                'column3' => [
                    'paddingTop' => "0",
                    'paddingBottom' => "0",
                    'paddingLeft' => "0",
                    'paddingRight' => "0",
                    'textAlign' => "left",
                    'verticalAlign' => "top",
                    'img' => [
                        'width' => "100%",
                        'height' => "auto"
                    ],
                    'content' => null
                ]
            ],
            'body' => [
                'backgroundColor' => '#ffffff',
                'color' => '#000000',
                'paddingTop' => "0",
                'paddingBottom' => "0",
                'paddingLeft' => "0",
                'paddingRight' => "0",
                'textAlign' => "left",
                'verticalAlign' => "top",
                'content' => null
            ]
        ];
    }

    /**
     * @inheritDoc
     */
    public function getTextWidgetView(?string $text): View
    {
        return view('laravel-pdf::pdf_templates.widgets.default.text', [
            'text' => $text
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getImageWidgetView(?string $alt, ?string $src): View
    {
        return view('laravel-pdf::pdf_templates.widgets.default.image', [
            'alt' => $alt,
            'src' => $src
        ]);
    }
}
