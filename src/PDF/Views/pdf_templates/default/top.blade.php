<style>
    .top {
        width: 100%;
        @isset($backgroundColor)
            background-color: {{ $backgroundColor }};
        @endisset
        @isset($color)
            color: {{ $color }};
        @endisset
    }
    .top table {
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0;
    }
</style>
<div class="top">
    <table>
        <tr>
            @isset($column1['content'])
                @include('laravel-pdf::pdf_templates.default.top.column', array_merge(['class' => 'column1'], $column1))
            @endisset
            @isset($column2['content'])
                @include('laravel-pdf::pdf_templates.default.top.column', array_merge(['class' => 'column2'], $column2))
            @endisset
            @isset($column3['content'])
                @include('laravel-pdf::pdf_templates.default.top.column', array_merge(['class' => 'column3'], $column3))
            @endisset
        </tr>
    </table>
</div>
