<style>
    .body {
        @isset($backgroundColor)
            background-color: {{ $backgroundColor }};
        @endisset
        @isset($color)
            color: {{ $color }};
        @endisset
        @isset($paddingTop)
            padding-top: {{ $paddingTop }};
        @endisset
        @isset($paddingBottom)
            padding-bottom: {{ $paddingBottom }};
        @endisset
        @isset($paddingLeft)
            padding-left: {{ $paddingLeft }};
        @endisset
        @isset($paddingRight)
            padding-right: {{ $paddingRight }};
        @endisset
        @isset($textAlign)
            text-align: {{ $textAlign }};
        @endisset
        @isset($verticalAlign)
            vertical-align: {{ $verticalAlign }};
        @endisset
    }
    /*.body table {
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0;
    }
    .body table td,
    .body table th{
        text-align: left;
        border: 1px solid black;
        padding: 8px;
    }*/
</style>
<div class="body">
    {!! $content !!}
</div>
