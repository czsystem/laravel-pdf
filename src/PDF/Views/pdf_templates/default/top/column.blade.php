<style>
    .top .{{ $class }} {
        @isset($paddingTop)
            padding-top: {{ $paddingTop }};
        @endisset
        @isset($paddingBottom)
            padding-bottom: {{ $paddingBottom }};
        @endisset
        @isset($paddingLeft)
            padding-left: {{ $paddingLeft }};
        @endisset
        @isset($paddingRight)
            padding-right: {{ $paddingRight }};
        @endisset
        @isset($textAlign)
            text-align: "{{ $textAlign }}";
        @endisset
        @isset($verticalAlign)
            vertical-align: "{{ $verticalAlign }}";
        @endisset
    }
    @isset($img)
        .top .{{ $class }} img {
            @isset($img['width'])
                width: {{ $img['width'] }};
            @endisset
            @isset($img['height'])
                height: {{ $img['height'] }};
            @endisset
        }
    @endisset
</style>
<td class="{{ $class }}">{!! $content !!}</td>
