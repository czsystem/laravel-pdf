@extends('laravel-pdf::template')
@section('body')
    <style>
        body {
            font-family: 'Nunito', sans-serif;
        }
    </style>
    @isset($top)
        @include('laravel-pdf::pdf_templates.default.top', $top)
    @endisset
    @isset($body)
        @include('laravel-pdf::pdf_templates.default.body', $body)
    @endisset
@endsection
