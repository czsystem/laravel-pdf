<?php

namespace CloudZentral\PDF\Interfaces;

use Illuminate\Http\Response;

/**
 * Interface PDFInterface
 * @package CloudZentral\PDF\Interfaces
 */
interface PDFInterface
{
    /**
     * Output PDF.
     * @return string
     */
    public function output(): string;

    /**
     * Download PDF.
     */
    public function download(): Response;

    /**
     * Save PDF.
     */
    public function save(): void;

    /**
     * Stream PDF.
     * @return Response
     */
    public function stream(): Response;
}
